package cellular.datastructure;

import java.util.ArrayList;
import java.util.List;

import cellular.CellState;


public class CellGrid implements IGrid {
	private List<CellState> cells;
	private int height;
	private int width;

	/**
	 * 
	 * Construct a grid with the given dimensions.
	 * 
	 * @param width
	 * @param height
	 * @param initElement
	 *            What the cells should initially hold (possibly null)
	 */
	public CellGrid(int width, int height, CellState initElement) {
		if(width <= 0 || height <= 0)
			throw new IllegalArgumentException();

		this.height = height;
		this.width = width;
		cells = new ArrayList<CellState>(height * width);
		for (int i = 0; i < height * width; ++i) {
			cells.add(initElement);
		}
	}

	
	@Override
	public int getHeight() {
		return height; // TODO: Task 1
	}


	@Override
	public int getWidth() {
		return width; // TODO: Task 1
	}


	@Override
	public void set(int x, int y, CellState elem) {
		if(x < 0 || x >= width)
			throw new IndexOutOfBoundsException();
		if(y < 0 || y >= height)
			throw new IndexOutOfBoundsException();

		cells.set(coordinateToIndex(x, y), elem);
	}

	private int coordinateToIndex(int x, int y) {
		return x + y*width; //TODO 
	}
	
	@Override
	public CellState get(int x, int y) {
		if(x < 0 || x >= width)
			throw new IndexOutOfBoundsException();
		if(y < 0 || y >= height)
			throw new IndexOutOfBoundsException();

		return cells.get(coordinateToIndex(x, y)); //TODO 
	}

	@Override
	public IGrid copy() {
		CellGrid newGrid = new CellGrid(getWidth(), getHeight(), null);
		for (int x = 0; x < width; x++)
			for(int y = 0; y < height; y++)
				newGrid.set(x,  y,  get(x, y));
		return newGrid;
	}

}
