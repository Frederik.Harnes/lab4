package cellular;

import java.awt.Color;
import java.util.Random;

/**
 * 
 * The State of a cell, either Alive or Dead
 */
public enum CellState {
	ALIVE,
	DEAD;

	public static CellState random(Random rand){
		return CellState.values()[rand.nextInt(2)];
	}

	/**
	 * Returns the cell state represented as a color
	 * 
	 *  ALIVE => BLACK 
	 *  DEAD => WHITE
	 *  
	 * @return black if the cell is alive, white otherwise
	 */
	public Color asColor() {
		switch(this) {
		case ALIVE : return Color.BLACK; 
		default : return Color.WHITE; 
		}
	}
}

